---
description: Presentación del curso
---

# 🚥 El Certificado IFCD0110

## Qué son los C**Ps (C**ertificados de Profesionalidad)

{% hint style="success" %}
Los certificados de profesionalidad son el instrumento de **acreditación oficial de las cualificaciones** profesionales del Catálogo Nacional de Cualificaciones Profesionales (CNCP) en el ámbito de la administración laboral. Se ordenan en 26 familias profesionales y tres niveles de cualificación.
{% endhint %}

{% hint style="info" %}
Cada certificado acredita el **conjunto de competencias profesionales** que capacitan para el desarrollo de una actividad laboral identificable en el sistema productivo.&#x20;

Referencia: [SEPE - Certificados de Profesionalidad](https://www.sepe.es/HomeSepe/Personas/formacion/certificados-profesionalidad)
{% endhint %}

Más info en: [Emprego Galicia - Qué son los Certificados de Profesionalidad](https://emprego.xunta.gal/portal/index.php/es/?option=com\_content\&view=article\&id=390)​

****

**Un certificado de profesionalidad otorga a la persona un título oficialmente reconocido que la acredita como profesional competente para ejercer una determinada profesión** incluida por el [INCUAL](https://incual.educacion.gob.es/informacion) en el Catálogo Nacional de Profesiones (CNP). El CNP recoge para todas las profesiones existentes, los conocimientos, aptitudes y comportamientos necesarios para desenvolverse con solvencia.

Los Certificados de Profesionalidad están organizados por **Módulos Formativos**, lo que posibilita establecer un itinerario personalizado de cualificación y permite a la Administración educativa convalidar las unidades de competencia que coincidan con los títulos de Formación Profesional, de manera que se facilita la consecución de un título reglado FP, superior.

![Relación entre los títulos FP y los CP](https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2Fmuv8njVLIlSMTMghSA3X%2Fuploads%2FQmrbj7DFVKF45LbN2WJy%2Fcatalog-modular.png?alt=media\&token=abce66d5-a8d3-4bc0-95ba-93182814d37a)

Estas equivalencias y acreditaciones también están reguladas mediante el Catálogo Nacional de Cualificaciones ([CNCP](https://incual.educacion.gob.es/bdc)).



## **Sobre el IFCD0110 (Certificado Profesional)**

**El IFCD0110 es un curso muy completo** que acredita como profesional competente para crear y publicar páginas web _responsive_, accesibles y _usables_, para desarrollar la actividad profesional tanto por cuenta propia como ajena, en empresas públicas o privadas que dispongan de infraestructura de redes intranet, Internet o extranet.

* [**Ficha del Certificado IFCD0110** en pdf](https://sepe.es/SiteSepe/contenidos/personas/formacion/certificados\_de\_profesionalidad/pdf/fichasCertificados/IFCD0110\_ficha.pdf)
* [**B.O.E. nº 300** del 14/12/2011](https://sede.sepe.gob.es/es/portaltrabaja/resources/pdf/especialidades/IFCD0110.pdf)

### Módulos y Unidades formativas

Listado del temario correspondiente al certificado de Confección y publicación de páginas web (IFCD0110)

## [**MF0950\_2**.- **Construcción de páginas web**](https://ifcd0110.gitbook.io/mf0950)

### [**UF1302**.- **Creación de páginas web con el lenguaje de marcas**](https://ifcd0110.gitbook.io/uf1302/) (80 h)

1. Los lenguajes de marcas
2. Imágenes y elementos multimedia
3. Técnicas de accesibilidad y usabilidad
4. Herramientas de edición web\


### ****[**UF1303**.- **Elaboración de hojas de estilo**](https://ifcd0110.gitbook.io/uf1303) (70 h)

1. Hojas de estilo en la construcción de páginas web
2. Diseño, ubicación y optimización de los contenidos de una página web\


### [**UF1304**.- **Elaboración de plantillas y formularios**](https://ifcd0110.gitbook.io/uf1304) (60 h)

1. Formularios en la construcción de páginas web
2. Plantillas en la construcción de páginas web\


## ****[**MF0951\_2**.- **Integración de componentes software en páginas web**](https://ifcd0110.gitbook.io/mf0951)****

### ****[**UF1305**.- **Programación con lenguajes de guión en páginas web**](https://ifcd0110.gitbook.io/uf1305) (90 h)

1. Metodología de la programación
2. Lenguaje de guión
3. Elementos básicos del lenguaje de guión
4. Desarrollo de scripts
5. Gestión de objetos del lenguaje de guión
6. Los eventos del lenguaje de guión
7. Búsqueda y análisis de scripts\


### ****[**UF1306**.- **Pruebas de funcionalidades y optimización de páginas web**](https://ifcd0110.gitbook.io/uf1306) (90 h)

1. Validaciones de datos en páginas web
2. Efectos especiales en páginas web
3. Pruebas y verificación en páginas web\
   \


## ****[**MF0952\_2**.- **Publicación de páginas web**](https://ifcd0110.gitbook.io/mf0952) (90 h)

1. Características de seguridad en la publicación de páginas web
2. Herramientas de transferencia de archivos
3. Publicación de páginas web
4. Pruebas y verificación de páginas web\


## **MP0278**.- **Módulo de prácticas profesionales no laborales** (80 h)
